/*
 * @copyright 	2012 (c) By Bright Solutions GmbH
 *  @author		Marc Sven Kleinboehl
 *
 */

(function ($) {
	
	Drupal.behaviors.updatescript = { attach: function(context) {
	 
			$('.updatescript-show-log-link', context).click(function(e) {
				
				jQuery("#updatescript-dialog").text (Drupal.settings.updatescript.wait_loading_log);
 
				jQuery("#updatescript-dialog").dialog({modal: true, maxHeight: 400, height: 400, title: 'Log' });
		 
				$.get(e.currentTarget.href, function(data) {
					$('#updatescript-dialog').html (data);
				});
				
				e.preventDefault();
				
				return false;
			});
			return;
		}
	};
})(jQuery)