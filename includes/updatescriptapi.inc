<?php
/*
 * @copyright 	2012 (c) By Bright Solutions GmbH
*  @author		Marc Sven Kleinboehl
*
* Contains some API host methods for the update scripts. 
*/

abstract class UpdateScriptAPI {

  protected abstract function log ($message);

  /*
   * Installs modules.
  * @param array $modules	An array of module names.
  */
  protected function setupModules (array $modules) {

    foreach ($modules as $module) {
      if (! module_enable (array($module))) {
        $this->log (t('Error while installing module @module.', array ('@module'=>$module)));
      }
    }

    return;
  }
   
  /*
   * Uninstalls modules.
  * @param array $modules	An array of module names.
  */
  protected function unsetupModules (array $modules) {
     
    foreach ($modules as $module) {
      if (! module_disable (array($module))) {
        $this->log (t('Error while uninstalling module @module.', array ('@module'=>$module)));
      }
    }
     
    return;
  }
   
  /*
   * Installs themes.
  * @param array $themes	An array of theme names.
  */
  protected function setupThemes (array $themes) {
     
    foreach ($themes as $theme) {
      if (! theme_disable (array($theme))) {
     	$this->log (t('Error while uninstalling theme @theme.', array ('@theme'=>$theme)));
      }
    }

    return;
  }

   
  /*
   * Uninstalls themes.
  * @param array $themes	An array of theme names.
  */
  protected function unsetupThemes (array $modules) {
     
    foreach ($themes as $theme) {
      if (! theme_disable (array($theme))) {
        $this->log (t('Error while uninstalling theme @theme.', array ('@theme'=>$theme)));
      }
    }
     
    return;
  }
   
  /*
   * Set the active theme.
  * @param string $themeName The name of the theme.
  * @param boolean $adminTheme (Optional)TRUE, if you want to set the admin theme.
  */
  protected function setDefaultTheme ($themeName, $adminTheme = false) {

    if ($adminTheme) {
      variable_set('admin_theme', $themeName);
    }else{
      variable_set('theme_default', $themeName);
    }
    
    return;
  }

  /*
   * Executes a drush command.
  * @param string $commandLine	The command to execute. May include placeholders used for sprintf.
  */
  protected function drush ($commandLine) {
     
    if (! drush_shell_exec('drush -y -r "' . getcwd() . '" ' . $commandLine)){
      $this->log (t('Can not execute drush command line.'));
    }else{
      $this->log (drush_shell_exec_output());
    }
     
    return;
  }
}

