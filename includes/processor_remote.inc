<?php
/*
 * @copyright 	2012 (c) By Bright Solutions GmbH
 * @author		Marc Sven Kleinboehl
 *
 * This include file contains all of the functions of the aspect for remote driven processing of the update scripts.
 */

/*
 * Checks whether the user is allowed to process the updatescripts.
 */
function _updatescript_processing_permitted ($submitted_key) {
  
  global $user;
  
  return (isset ($user->uid) && $user->uid == 1) || $submitted_key ==  drupal_get_private_key ();
}

/*
 * Sends a HTTP response message.
 * @param integer	$status		The HTTP status code..
 * @param string	$message	The message.
 */
function _updatescript_send_HTTP_response ($status, $message) {
  
  static $status_codes = array (
    '200' => 'OK',
    '304' => 'Not modified',
    '403' => 'Forbidden',
  	'404' => 'Not found'    
  );
  
  $status = $status . ' ' . $status_codes[$status];

  header('HTTP/1.0 ' . $status);
  header('Status: ' . $status);
  header('Content-Type: text/plain');
  
  print ($message);
  
  return;
}

/*
 * Starts an updatescript process.
 * @param string $drupal_private_key	The private key of the drupal installation for authorisation issue.
 */
function _updatescript_process ($drupal_private_key) {
  
  if (! _updatescript_processing_permitted ($drupal_private_key)) {
    if (php_sapi_name() != 'cli') {
      _updatescript_send_HTTP_response (403, t('Access denied.'));
    }else{
      print (t('Access denied'));
    }
    return;
  }
  
  module_load_include ('inc', 'updatescript', 'includes/register');
  
  $unprocessed_scripts = _updatescript_detect_unprocessed_scripts ();
 
  if (empty ($unprocessed_scripts)) {
    if (php_sapi_name() != 'cli') {
     _updatescript_send_HTTP_response (304, t('No new scripts available.'));
    }else{
      print(t('No new scripts available.'));
    }
    return;
  }
  
  variable_set ('site_offline' , '1');
  
  module_load_include ('inc', 'updatescript', 'includes/updatescriptentity');
  
  foreach ($unprocessed_scripts as $script) {
    _updatescript_run_script ($script);
  }
  
  menu_cache_clear_all();
  cache_clear_all();
  
  variable_set ('site_offline' , '0');

  if (php_sapi_name() != 'cli') {
    _updatescript_send_HTTP_response (200, t('Ok'));
  }else{
    print (t('Ok'));
  }
  
  return null;
}

/*
 * Runs a single script.
 * @param string	$script	The filename of the script.
 */
function _updatescript_run_script ($script) {
  
  $scriptEntity = UpdateScriptEntity::load($script);    
  $scriptEntity->saveEntity ();
  unset ($scriptEntity);
  
  return;
}