<?php
/*
 * @copyright 	2012 (c) By Bright Solutions GmbH
 * @author		Sebastian Kauschke
 *
 * @file 
 * 
 * Allows poor windows users to execute the updatescript processing via drush.
 * 
 * For more informations, run: drush runbsu _h
 */

// Declaring the commandline arguments.
define ('updatescript_CMD_ARG_SPECIFIC_FILE', '_d');
define ('updatescript_CMD_ARG_SILENT', '_s');
define ('updatescript_CMD_ARG_HELP', '_h');


/*
 * Shows drush commands to drush.
 * @return array	An array which contains informations about the drush command items.
 */
function updatescript_drush_command() {
  $items = array();

  $items['updatescripts'] = array(
      'description' => 'Run BS updatescripts',
      'aliases'     => array('runbsu'),
  );

  return $items;
}

/*
 * Help Information for drush commands
 * @param string $section 	Specifies the help document section.
 */
function updatescript_drush_help($section) {
  
  switch ($section) {
    case 'drush:updatescripts':
      return "Execute BS Updatescripts. You can also use the short version 'drush runbsu'";
      break;
  }
  
  return;
}


/*
 * drush updatescripts runs the updatescripts.
 */
function drush_updatescript_updatescripts() {
  
  $commandline_arguments = _updatescript_get_arguments ();

  // Was the help document ordered?
  if ($commandline_arguments[updatescript_CMD_ARG_HELP]) {
    _updatescript_render_help ();
    return;
  }  
 
  variable_set('site_offline', '1'); // Lock site.
  
  // Normal mode or has a specific file to process?
  if (! isset ($commandline_arguments[updatescript_CMD_ARG_SPECIFIC_FILE])) {
    
    module_load_include ('inc', 'updatescript', 'includes/process_remote');
    
    $message = _updatescript_process (drupal_get_private_key ());

  }else{
    _updatescript_run_script ($commandline_arguments[updatescript_CMD_ARG_SPECIFIC_FILE]);
    $message = 'A single script has been processed.';
  }
  
  variable_set('site_offline', '0'); // Unlock site.

  drush_shell_exec("drush clear-cache all");
  
  if (! $commandline_arguments[updatescript_CMD_ARG_SILENT]) {
    drush_print($message);
  }
  
  return;
}

/*
 * Retrieves the list of commandline arguments.
 * @return array 	An array which contains the commandline arguments.
 */
function _updatescript_get_arguments () {
  
  // Set defaults:
  $arguments[updatescript_CMD_ARG_SILENT] = false;
  $arguments[updatescript_CMD_ARG_HELP]   = false;
  
  // Fetch argument informations.
  $arguments_amount = $_SERVER['argc'];  
  $arguments_values = array ();
  	
  // No arguments? Nothing to do!
  if ($arguments_amount == 0) {
    return $arguments_values; 
  }
  
  $i = 0;
	
  while ($i < $arguments_amount) {
 
    $current_argument = $_SERVER['argv'][$i];
    
    switch ($current_argument) {
      case updatescript_CMD_ARG_SPECIFIC_FILE:        
        $i++;
        $arguments[$current_argument] = $_SERVER['argv'][$i];
        break;

      case updatescript_CMD_ARG_HELP:
      case updatescript_CMD_ARG_SILENT:
        $arguments[$current_argument] = true;
        break;
        
      // EXTEND HERE FOR FUTURE COMMANDLINE ARGUMENT EXTENDING!!!
    }
    
    $i++;
  }
  
  return $arguments;
}

/*
 * Renders a help document out to the console.
 */
function _updatescript_render_help () {
  
  drush_print("BS Updatescript processor V3");
  drush_print("2012(c) By Bright Solutions GmbH");
  drush_print("All rights reserved\n");
  drush_print("-s\tSilent mode (no output).");
  drush_print("-d\tProcess a specific file.");
  drush_print("-h\tThis document.");
   
  return;
}